We are always appreciative of suggestions and comments on the benchmark set!  If
you would like to contact us with suggestions, please e-mail us at
<elsi-team@duke.edu>.

If you would like to contribute to the ELSI performance benchmark set (or any
other ELSI-related project) directly, please contact us at the above e-mail
address to receive access to our GitLab.
