#!/bin/bash

# Prototype for script to create a test set for a user-specified range of the number of atoms
# This version only creates periodic test cases where all supercell dimensions are the same
# In addition, it only creates FHI-aims control.in files for PBE and tight settings

MIN_N_ATOMS=$1
MAX_N_ATOMS=$2

ROOT_FOLDER=BenchmarkSet

# Error checking
if [[ $# -lt 2 ]] 
then
  echo "Please specify two arguments, the minimum number of atoms and the maximum number of atoms.  Exiting."
  exit 1
fi
if [[ $MIN_N_ATOMS -lt 0 ]] 
then
  echo "The minimum number of atoms must be non-negative.  Please specify a valid number.  Exiting."
  exit 1
fi

if [[ $MAX_N_ATOMS -le 0 ]] 
then
  echo "The maximum number of atoms must be positive.  Please specify a valid number.  Exiting."
  exit 1
fi
if [[ $MAX_N_ATOMS -lt $MIN_N_ATOMS ]]
then
  echo "The specified minimum number of atoms is greater than the maximum number of atoms.  Please specify valid numbers.  Exiting."
  exit 1
fi

if [[ -a $ROOT_FOLDER ]]
then
  echo "Cowardly refusing to overwrite ${ROOT_FOLDER}, please remove the pre-existing directory and try again.  Exiting."
  exit 4
fi

# Loop over all geometries, determining if supercells could be created that match the user's
# specified range for number of atoms
for class in Periodic NonPeriodic
do
  for subclass in $class/*/
  do
    for testcase in $subclass/*/
    do
      BASE_GEO=$testcase/geometry.in
      N_ATOMS_BASE=$(grep -E "^ *atom" $BASE_GEO | wc | awk '{print $1}')

      # Determine the scaling dimensionality based on the metadata in the geometry file
      if [[ -n $(grep "Keywords" $BASE_GEO | grep "0D") ]] 
      then
        DIMENSION=0
        X_PERIODIC=0
        Y_PERIODIC=0
        Z_PERIODIC=0
        MAX_DIM=1
      elif [[ -n $(grep "Keywords" $BASE_GEO | grep "1D_Z") ]] 
      then
        DIMENSION=1
        X_PERIODIC=0
        Y_PERIODIC=0
        Z_PERIODIC=1
        MAX_DIM=$MAX_N_ATOMS
      elif [[ -n $(grep "Keywords" $BASE_GEO | grep "2D_XY") ]] 
      then
        DIMENSION=2
        X_PERIODIC=1
        Y_PERIODIC=1
        Z_PERIODIC=0
        MAX_DIM=$MAX_N_ATOMS
      elif [[ -n $(grep "Keywords" $BASE_GEO | grep "3D") ]] 
      then
        DIMENSION=3
        X_PERIODIC=1
        Y_PERIODIC=1
        Z_PERIODIC=1
        MAX_DIM=$MAX_N_ATOMS
      else 
        # At the time of this writing, no 1D_X, 1D_Y, 2D_XZ, or 2D_YZ materials exist in the test set, but perform a check in case
        # they are added in the future.
        echo "Scaling dimensionality of $BASE_GEO is not provided or is not supported by this script.  Skipping this material."
        continue
      fi

      # Loop over uniformly-dilated supercells.
      for i in $(seq 1 1 $MAX_DIM)
      do
        # Determine what the proper supercell dimensions are, based on scaling dimensionality
        if [[ $X_PERIODIC = 1 ]]
        then
          X_DIM=$i
        else
          X_DIM=1
        fi
        if [[ $Y_PERIODIC = 1 ]]
        then
          Y_DIM=$i
        else
          Y_DIM=1
        fi
        if [[ $Z_PERIODIC = 1 ]]
        then
          Z_DIM=$i
        else
          Z_DIM=1
        fi

        N_ATOMS=$(($N_ATOMS_BASE*$X_DIM*$Y_DIM*$Z_DIM))

        # If the number of atoms in the supercell matches the user's criteria, create it
        if [[ $N_ATOMS -ge $MIN_N_ATOMS && $N_ATOMS -le $MAX_N_ATOMS ]] 
        then
          # Note that I create a trivial 1x1x1 "supercell" folder for non-periodic calculations.  This is done 
          # deliberately, and not out of scripting laziness, to keep the folder structure consistent.  I may add 
          # the ability to generate a periodic calculation from a non-periodic geometry.
          SUPERCELL_FOLDER=${ROOT_FOLDER}/$testcase/${X_DIM}x${Y_DIM}x${Z_DIM}
          echo "Creating $testcase calculation with $N_ATOMS atoms"
          mkdir -p $SUPERCELL_FOLDER

          # Determine whether a calculation is periodic or non-periodic based on the existance of lattice vectors
          # For periodic systems, make supercells.  For non-periodic systems, simply copy the geometry over.
          # TODO:  Check error code of GenerateMaterials.py to make sure that nothing failed
          if [[ -n $(grep "lattice_vector" $BASE_GEO) ]]
          then
            ./GenerateMaterial.py $BASE_GEO $X_DIM $Y_DIM $Z_DIM > ${SUPERCELL_FOLDER}/geometry.in
          else
            cp $BASE_GEO ${SUPERCELL_FOLDER}/geometry.in
          fi

          cat Periodic/control.in.pbe $testcase/control.in.tight > ${SUPERCELL_FOLDER}/control.in
        fi
      done
    done
  done
done

# Inform user if no benchmark set was created and throw an error code
if [[ ! -a $ROOT_FOLDER ]]
then
  echo "No geometries could be created that matched your specifications.  A benchmark set was not created.  Exiting."
  exit 5
fi
