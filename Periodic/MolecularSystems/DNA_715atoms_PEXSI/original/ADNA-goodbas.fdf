# FDF input file for A-DNA (11 GC base pairs) Relaxed and symmetrised.
# Emilio Artacho 1998
# Improved basis. Emilio Artacho 2004

SystemName         A-DNA. 11 GC base pairs. Marivi's optimised basis 0.2 GPa
SystemLabel        ADNA-goodbas

WriteDMHS.NetCDF F
WriteDM.NetCDF F
WriteDM T
save-hs F

ElectronicTemperature   300.0 K

SolutionMethod        pexsi
MaxSCFIterations      10
DM.Tolerance          1.d-5

MPI.Nprocs.SIESTA  64
PEXSI.np-per-pole  4
PEXSI.np-symbfact  4

# PEXSI variables
PEXSI.delta-E            20.0 Ry
PEXSI.ordering           0
PEXSI.inertia-count      1
PEXSI.inertia-counts     10
PEXSI.inertia-num-electron-tolerance 600
PEXSI.mu-max-iter 5
PEXSI.num-poles 50
PEXSI.muMin -2.00 Ry
PEXSI.muMax 0.00 Ry
PEXSI.num-electron-tolerance 0.001
DM.NormalizeDuringSCF   .false.
DM.NormalizationTolerance 1d-1  # (true_no_electrons/no_electrons) - 1.0 


# Species parameters
NumberOfSpecies    5
%block ChemicalSpeciesLabel
  1   1   H
  2   6   C
  3   7   N
  4   8   O
  5  15   P
%endblock ChemicalSpeciesLabel

%block SuperCell
  1   0   0
  0   1   0
  0   0   1
%endblock SuperCell

# Basis set options

PAO.SplitNorm 0.3
%Block PAO.Basis
P   2
 n=3   0   2
    6. 0.
 n=3   1   2   P
    6. 0.
H    2      0.76857
 n=1   0   2   E    45.37962     4.20457
     6.06811     1.85004
 n=2   1   1   E    40.08175     2.95945
     4.74784
C    3      0.09879
 n=2   0   2   E    39.37755     3.60560
     5.20021     2.85570
 n=2   1   2   E    95.68842     4.31202
     5.68648     2.94880
 n=3   2   1   E    62.49576     0.66804
     3.97291
N    3     -0.00139
 n=2   0   2   E    65.50216     4.29661
     5.64483     3.02914
 n=2   1   2   E    30.54417     5.81284
     6.20000     2.85547
 n=3   2   1   E    59.15335     0.14049
     3.65788
O    3     -0.24021
 n=2   0   2   E    64.35698     2.24090
     4.39710     2.52029
 n=2   1   2   E     7.99069     4.88285
     6.12535     2.30982
 n=3   2   1   E    20.95104     3.07361
     5.07487
%EndBlock PAO.Basis

# Unit cell and atomic positions
LatticeConstant                   < ApGpC_xyz_goodbas.fdf
LatticeVectors                    < ApGpC_xyz_goodbas.fdf
NumberOfAtoms                     < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesFormat           < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesOrigin           < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesAndAtomicSpecies < ApGpC_xyz_goodbas.fdf

XC.Functional         GGA
XC.Authors            PBE

MeshCutoff           140. Ry
#NeglNonOverlapInt     yes


#UseDomainDecomposition      .true.



# Diagon options
ElectronicTemperature  5 meV 

# SCF options
DM.UseSaveDM          yes
DM.MixingWeight       0.10
DM.NumberPulay        3
#DM.PulayOnFile        true

# Order-N Options
#ON.functional         Kim
#ON.UseSaveLWF         yes
#ON.MaxNumIter         2000
#ON.etol               2.d-8
#ON.eta               -1.46 eV
#ON.RcLWF              4.0 Ang

# MD options
MD.TypeOfRun          cg
MD.NumCGsteps         0
MD.MaxCGDispl         0.06 Ang 
MD.MaxForceTol        0.04 eV/Ang  
MD.UseSaveXV          yes
MD.UseSaveCG          yes

# Output options
# SaveRho
# SaveElectrostaticPotential
# WriteCoorStep
